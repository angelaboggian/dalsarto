<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_1_3_2($object)
{
    $result = true;

    $result &= Configuration::updateGlobalValue('STSN_PRODUCT_VIEW_MOBILE', 'list_view');
    
	return $result;
}
