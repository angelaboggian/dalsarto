<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_1_3_5($object)
{
    $result = true;

    $result &= Configuration::updateGlobalValue('STSN_CATE_LABEL_FONT_MENU', '');
    $result &= Configuration::updateGlobalValue('STSN_CATE_LABEL_FONT_MENU_SIZE', 0);
    $result &= Configuration::updateGlobalValue('STSN_TOPBAR_LEFT_DROP_DOWN_ALIGN', 0);
    $result &= Configuration::updateGlobalValue('STSN_TOPBAR_RIGHT_DROP_DOWN_ALIGN', 0);
    
	return $result;
}
