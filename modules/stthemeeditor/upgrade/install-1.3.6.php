<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_1_3_6($object)
{
    $result = true;

    $result &= Configuration::updateGlobalValue('STSN_HIDE_GENDER', 0);
    $result &= Configuration::updateGlobalValue('STSN_HIDE_DATE_OF_BIRTH', 0);
    $result &= Configuration::updateGlobalValue('STSN_MOBILE_LOGO', '');
    $result &= Configuration::updateGlobalValue('STSN_MOBILE_LOGO_WIDTH', 0);
    $result &= Configuration::updateGlobalValue('STSN_MOBILE_LOGO_HEIGHT', 0);
    
	return $result;
}
