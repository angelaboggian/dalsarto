<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_1_4_1($object)
{
    $result = true;
            
    $field = Db::getInstance()->executeS('Describe `'._DB_PREFIX_.'st_news_letter` `id_gender`');  
   
    if(!is_array($field) || !count($field))
        if (!Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'st_news_letter` 
            ADD `id_gender` int(10) unsigned NOT NULL DEFAULT 0'))
            $result &= false;

	return $result;
}
