<?php
class jscomposervc_contentanywhereModuleFrontController extends ModuleFrontController   {
 	public function init()
	{
		// print Tools::getValue('controller');
		$this->display_column_left = false;
		$this->display_column_right = false;
		$this->display_header = true;
		$this->display_footer = true;
		$this->controller_type = 'admin';
		$this->val_identifier = Tools::getValue('val_identifier');
		parent::init();
	}
    public function initContent() {
		parent::initContent();
		$contenttext = Hook::exec('displayBackOfficeHeader');
		$this->context->controller->addJqueryUI(array('ui.tabs', 'ui.widget', 'ui.sortable', 'ui.droppable', 'ui.draggable', 'ui.accordion', 'ui.autocomplete', 'ui.slider'));

		$content_text = $this->content_text();
		JsComposer::loadFrontendEditorHead(htmlentities($content_text));
		$contenttext .=  $content_text;

		$contenttext .= Hook::exec('displayBackOfficeFooter');
		$this->context->smarty->assign(array('content'=>$contenttext));
		$this->setTemplate('vc_contentanywhere.tpl');
    }
	public function content_text()
    {
        ob_start();
        $Vc_Frontend_Editor = new Vc_Frontend_Editor();
        $Vc_Frontend_Editor->contentAnyWhereInit($this->val_identifier);
        return ob_get_clean();
    }
}