{if isset($stmenu) && is_array($stmenu) && count($stmenu)}
{if $header_bottom}
<div id="st_mega_menu_container" class="animated fast">
	<div class="container">
{/if}
	<nav id="st_mega_menu_wrap" role="navigation">
		{include file="./stmegamenu-ul.tpl" is_mega_menu_main=1}
	</nav>
{if $header_bottom}
	</div>
</div>
{/if}
{/if}