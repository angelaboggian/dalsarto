<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_2_0_8($object)
{
    $result = true;

    $field = Db::getInstance()->executeS('Describe `'._DB_PREFIX_.'st_mega_menu` `granditem`');  
   
    if(!is_array($field) || !count($field))
        $result &= Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'st_mega_menu` ADD `granditem` tinyint(1) DEFAULT 0');
        
	return $result;
}