<!-- Block user information module NAV  -->
{assign var='userinfo_dropdown' value=Configuration::get('ST_USERINFO_DROPDOWN')}
{assign var='show_user_info_icons' value=Configuration::get('ST_SHOW_USER_INFO_ICONS')}
{assign var='welcome_logged' value=Configuration::get('STSN_WELCOME_LOGGED', $lang_id)}
{assign var='welcome_link' value=Configuration::get('STSN_WELCOME_LINK', $lang_id)}
{assign var='welcome' value=Configuration::get('STSN_WELCOME', $lang_id)}
{if $is_logged}
	{if isset($userinfo_navleft) && $userinfo_navleft}
		{if isset($welcome_logged) && trim($welcome_logged)}{if $welcome_link}<a href="{$welcome_link}" class="welcome top_bar_item {if !isset($show_welcome_msg) || !$show_welcome_msg} hidden_extra_small {/if}" rel="nofollow" title="{$welcome_logged}">{else}<span class="welcome top_bar_item {if !isset($show_welcome_msg) || !$show_welcome_msg} hidden_extra_small {/if}">{/if}<span class="header_item">{$welcome_logged}</span>{if $welcome_link}</a>{else}</span>{/if}{/if}
		{if $userinfo_dropdown}
			<div class="userinfo_mod_top dropdown_wrap top_bar_item">
		        <div class="dropdown_tri dropdown_tri_in header_item">
		            <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" rel="nofollow">
		        		{if $show_user_info_icons}<i class="icon-user-1 icon-mar-lr2 icon-large"></i>{/if}{$cookie->customer_firstname} {$cookie->customer_lastname}
		            </a>
		        </div>
		        <div class="dropdown_list">
            		<ul class="dropdown_list_ul custom_links_list">
            			<li><a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" rel="nofollow">{l s='My Account' mod='blockuserinfo'}</a></li>
						<li><a href="{$link->getPageLink('index', true, NULL, 'mylogout')|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">{l s='Sign out' mod='blockuserinfo'}</a></li>
		    		</ul>
		        </div>
		    </div>
		{else}
			<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account top_bar_item" rel="nofollow"><span class="header_item">{if $show_user_info_icons}<i class="icon-user-1 icon-mar-lr2 icon-large"></i>{/if}{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
			<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="my_account_link top_bar_item" rel="nofollow"><span class="header_item">{l s='My Account' mod='blockuserinfo'}</span></a>
			<a class="logout top_bar_item" href="{$link->getPageLink('index', true, NULL, 'mylogout')|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
				<span class="header_item">{if $show_user_info_icons}<i class="icon-logout icon-large"></i>{/if}{l s='Sign out' mod='blockuserinfo'}</span>
			</a>
		{/if}
	{else}
		{if $userinfo_dropdown}
			<div class="userinfo_mod_top dropdown_wrap top_bar_item">
		        <div class="dropdown_tri dropdown_tri_in header_item">
		            <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" rel="nofollow">
		        		{if $show_user_info_icons}<i class="icon-user-1 icon-mar-lr2 icon-large"></i>{/if}{$cookie->customer_firstname} {$cookie->customer_lastname}
		            </a>
		        </div>
		        <div class="dropdown_list">
            		<ul class="dropdown_list_ul custom_links_list">
            			<li><a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" rel="nofollow">{l s='My Account' mod='blockuserinfo'}</a></li>
						<li><a href="{$link->getPageLink('index', true, NULL, 'mylogout')|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">{l s='Sign out' mod='blockuserinfo'}</a></li>
		    		</ul>
		        </div>
		    </div>
		{else}
			<a class="logout top_bar_item" href="{$link->getPageLink('index', true, NULL, 'mylogout')|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
				<span class="header_item">{if $show_user_info_icons}<i class="icon-logout icon-large"></i>{/if}{l s='Sign out' mod='blockuserinfo'}</span>
			</a>
			<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="my_account_link top_bar_item" rel="nofollow"><span class="header_item">{l s='My Account' mod='blockuserinfo'}</span></a>
			<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account top_bar_item" rel="nofollow"><span class="header_item">{if $show_user_info_icons}<i class="icon-user-1 icon-mar-lr2 icon-large"></i>{/if}{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
		{/if}
		{if isset($welcome_logged) && trim($welcome_logged)}{if $welcome_link}<a href="{$welcome_link}" class="welcome top_bar_item {if !isset($show_welcome_msg) || !$show_welcome_msg} hidden_extra_small {/if}" rel="nofollow" title="{$welcome_logged}">{else}<span class="welcome top_bar_item {if !isset($show_welcome_msg) || !$show_welcome_msg} hidden_extra_small {/if}">{/if}<span class="header_item">{$welcome_logged}</span>{if $welcome_link}</a>{else}</span>{/if}{/if}
	{/if}
	<div id="for_mobile_userinfo" class="hidden">
		{if isset($welcome_logged) && trim($welcome_logged)}
	    <li class="mo_ml_level_0 mo_ml_column">
	        <a href="{if $welcome_link}{$welcome_link}{else}javascript:;{/if}" rel="nofollow" class="mo_ma_level_0 {if !$welcome_link} ma_span{/if}" title="{$welcome_logged}">
	            {$welcome_logged}
	        </a>
	    </li>
	    {/if}
	    <li class="mo_ml_level_0 mo_ml_column">
	        <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" class="mo_ma_level_0" title="{l s='View my customer account' mod='blockuserinfo'}">
	            {if $show_user_info_icons}<i class="icon-user-1 icon-mar-lr2 icon-large"></i>{/if}{$cookie->customer_firstname} {$cookie->customer_lastname}
	        </a>
	    </li>
	    <li class="mo_ml_level_0 mo_ml_column">
	        <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" class="mo_ma_level_0" title="{l s='View my customer account' mod='blockuserinfo'}">
	            {l s='My Account' mod='blockuserinfo'}
	        </a>
	    </li>
	    <li class="mo_ml_level_0 mo_ml_column">
	        <a href="{$link->getPageLink('index', true, NULL, 'mylogout')|escape:'html':'UTF-8'}" rel="nofollow" class="mo_ma_level_0" title="{l s='Log me out' mod='blockuserinfo'}">
	            {if $show_user_info_icons}<i class="icon-logout icon-large"></i>{/if}{l s='Sign out' mod='blockuserinfo'}
	        </a>
	    </li>
	</div>
{else}
	{if isset($userinfo_navleft) && $userinfo_navleft}
		{if isset($welcome) && trim($welcome)}{if $welcome_link}<a href="{$welcome_link}" class="welcome top_bar_item {if !isset($show_welcome_msg) || !$show_welcome_msg} hidden_extra_small {/if}" rel="nofollow" title="{$welcome}">{else}<span class="welcome top_bar_item {if !isset($show_welcome_msg) || !$show_welcome_msg} hidden_extra_small {/if}">{/if}<span class="header_item">{$welcome}</span>{if $welcome_link}</a>{else}</span>{/if}{/if}
		<a class="login top_bar_item" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			<span class="header_item">{if $show_user_info_icons}<i class="icon-user-1 icon-mar-lr2 icon-large"></i>{/if}{l s='Login' mod='blockuserinfo'}</span>
		</a>
	{else}
		<a class="login top_bar_item" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			<span class="header_item">{if $show_user_info_icons}<i class="icon-user-1 icon-mar-lr2 icon-large"></i>{/if}{l s='Login' mod='blockuserinfo'}</span>
		</a>
		{if isset($welcome) && trim($welcome)}{if $welcome_link}<a href="{$welcome_link}" class="welcome top_bar_item {if !isset($show_welcome_msg) || !$show_welcome_msg} hidden_extra_small {/if}" rel="nofollow" title="{$welcome}">{else}<span class="welcome top_bar_item {if !isset($show_welcome_msg) || !$show_welcome_msg} hidden_extra_small {/if}">{/if}<span class="header_item">{$welcome}</span>{if $welcome_link}</a>{else}</span>{/if}{/if}
	{/if}
{/if}
<!-- /Block usmodule NAV -->
