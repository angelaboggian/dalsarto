<?php
/*
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http:* If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http:*
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2015 PrestaShop SA
 *  @license    http:*  International Registered Trademark & Property of PrestaShop SA
 */
class CategoryController extends CategoryControllerCore {
	  /*
    * module: stoverride
    * date: 2017-12-08 19:09:01
    * version: 1.2.0
    */
    /*
    * module: jscomposer
    * date: 2016-02-09 03:58:59
    * version: 4.3.13
    */
    /*
    * module: jscomposer
    * date: 2017-12-08 20:31:33
    * version: 4.4.8
    */
    public function initContent() {
        parent::initContent();
        $this->context->smarty->assign(array(   
            'HOOK_CATEGORY_HEADER' => Hook::exec('displayCategoryHeader'),    
						'HOOK_CATEGORY_FOOTER' => Hook::exec('displayCategoryFooter'),     
            'display_category_title' => Configuration::get('STSN_DISPLAY_CATEGORY_TITLE'),  
            'display_category_image' => Configuration::get('STSN_DISPLAY_CATEGORY_IMAGE'),  
            'display_category_desc' => Configuration::get('STSN_DISPLAY_CATEGORY_DESC'),
            'display_subcategory' => Configuration::get('STSN_DISPLAY_SUBCATE'),  
						'categorySize' => Image::getSize(ImageType::getFormatedName('category')),  
				));
        $description = $this->category->description = JsComposer::do_shortcode($this->category->description);
        parent::initContent();
        if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer')) {
            $this->context->smarty->assign(
                array(
                    'description_short' => JsComposer::do_shortcode($description),
                )
            );
            $this->category->description = JsComposer::do_shortcode($description);
        }
        if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode')) {
            
            $this->context->smarty->assign(
                array(
                    'description_short' => smartshortcode::do_shortcode($description),                
                )
            );
            $this->category->description = smartshortcode::do_shortcode($description);            
        }
    }
}
