<?php
class ProductController extends ProductControllerCore
{
    /*
    * module: stoverride
    * date: 2017-12-08 19:09:02
    * version: 1.2.0
    */
    public function initContent()
    {
        parent::initContent();
        $this->context->smarty->assign(array(   
            'HOOK_PRODUCT_SECONDARY_COLUMN' => Hook::exec('displayProductSecondaryColumn'),     
        ));
    }
	/*
    * module: jscomposer
    * date: 2016-02-09 03:59:00
    * version: 4.3.13
    */
    /*
    * module: jscomposer
    * date: 2017-12-08 20:31:33
    * version: 4.4.8
    */
    public function display()
	{
            if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer'))
            {
                   $this->product->description = JsComposer::do_shortcode( $this->product->description );
            }
            if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
            {
                   $this->product->description = smartshortcode::do_shortcode( $this->product->description );
            }
            return parent::display();
	}
}
