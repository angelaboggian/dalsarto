<?php /* Smarty version Smarty-3.1.19, created on 2017-12-20 14:44:04
         compiled from "/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/stcountdown/views/templates/hook/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5560135205a3a6924e12b13-57353202%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '89ff31a5fbb39fb63cc8708426edf68b58538108' => 
    array (
      0 => '/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/stcountdown/views/templates/hook/header.tpl',
      1 => 1512756515,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5560135205a3a6924e12b13-57353202',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'custom_css' => 0,
    'countdown_active' => 0,
    'display_all' => 0,
    'id_products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3a6924e54a82_97219962',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3a6924e54a82_97219962')) {function content_5a3a6924e54a82_97219962($_smarty_tpl) {?>
<?php if (isset($_smarty_tpl->tpl_vars['custom_css']->value)&&$_smarty_tpl->tpl_vars['custom_css']->value) {?>
<style type="text/css"><?php echo $_smarty_tpl->tpl_vars['custom_css']->value;?>
</style>
<?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['countdown_active']->value)&&$_smarty_tpl->tpl_vars['countdown_active']->value) {?>
<script type="text/javascript">
//<![CDATA[

var s_countdown_all = <?php echo $_smarty_tpl->tpl_vars['display_all']->value;?>
;
var s_countdown_id_products = [<?php echo $_smarty_tpl->tpl_vars['id_products']->value;?>
];
var s_countdown_start = function(){
    $('.s_countdown_block .s_countdown_timer, .c_countdown_timer').each(function() {
        var that = $(this), finalDate = $(this).data('countdown'), gmDate = $(this).data('gmdate'), id = that.data('id-product'), countdown_pro = $(this).hasClass('countdown_pro');
        var s_countdown_now = new Date();
        var s_countdown_utc = new Date(s_countdown_now.getTime() + s_countdown_now.getTimezoneOffset() * 60000);
        if ((s_countdown_all || $.inArray(id, s_countdown_id_products) > -1) && (new Date(gmDate)>s_countdown_utc))
        {
            that.countdown(new Date(new Date(gmDate).getTime() - s_countdown_now.getTimezoneOffset() * 60000)).on('update.countdown', function(event) {
                
                <?php if (Configuration::get('ST_COUNTDOWN_STYLE')==1) {?>
                var format = '<div><i class="icon-clock"></i>%D '+((event.offset.totalDays == 1) ? "<?php echo smartyTranslate(array('s'=>'day','mod'=>'stcountdown'),$_smarty_tpl);?>
" : "<?php echo smartyTranslate(array('s'=>'days','mod'=>'stcountdown'),$_smarty_tpl);?>
")+' <span class="for_c_time_style">%H : %M : %S</span></div>';
                if(countdown_pro)
                    format = '%D '+((event.offset.totalDays == 1) ? "<?php echo smartyTranslate(array('s'=>'day','mod'=>'stcountdown'),$_smarty_tpl);?>
" : "<?php echo smartyTranslate(array('s'=>'days','mod'=>'stcountdown'),$_smarty_tpl);?>
")+' <span class="for_c_time_style">%H : %M : %S</span>';
                <?php } else { ?>
                var format = '<div><span class="countdown_number">%D</span><span class="countdown_text">'+((event.offset.totalDays == 1) ? "<?php echo smartyTranslate(array('s'=>'day','mod'=>'stcountdown'),$_smarty_tpl);?>
" : "<?php echo smartyTranslate(array('s'=>'days','mod'=>'stcountdown'),$_smarty_tpl);?>
")+'</span></div><div><span class="countdown_number">%H</span><span class="countdown_text"><?php echo smartyTranslate(array('s'=>'hrs','mod'=>'stcountdown'),$_smarty_tpl);?>
</span></div><div><span class="countdown_number">%M</span><span class="countdown_text"><?php echo smartyTranslate(array('s'=>'min','mod'=>'stcountdown'),$_smarty_tpl);?>
</span></div><div><span class="countdown_number">%S</span><span class="countdown_text"><?php echo smartyTranslate(array('s'=>'sec','mod'=>'stcountdown'),$_smarty_tpl);?>
</span></div>';
                if(countdown_pro)
                    format = '%D '+((event.offset.totalDays == 1) ? "<?php echo smartyTranslate(array('s'=>'day','mod'=>'stcountdown'),$_smarty_tpl);?>
" : "<?php echo smartyTranslate(array('s'=>'days','mod'=>'stcountdown'),$_smarty_tpl);?>
")+' %H <?php echo smartyTranslate(array('s'=>'hrs','mod'=>'stcountdown'),$_smarty_tpl);?>
 %M <?php echo smartyTranslate(array('s'=>'min','mod'=>'stcountdown'),$_smarty_tpl);?>
 %S <?php echo smartyTranslate(array('s'=>'sec','mod'=>'stcountdown'),$_smarty_tpl);?>
';
                <?php }?>
                
                that.html(event.strftime(format));
            }).on('finish.countdown',function(event){
                window.location.reload(true);
            });
            if(countdown_pro)
                that.closest('.countdown_outer_box').addClass('counting');
            else
                that.addClass('counting');
        }
    });
    $('.s_countdown_block .s_countdown_perm, .c_countdown_perm, .countdown_pro_perm').each(function() {
        if (s_countdown_all || $.inArray($(this).data('id-product'), s_countdown_id_products) > -1)
            $(this).addClass('counting');
    });
};
jQuery(function($) {
    s_countdown_start();
});    
 
//]]>
</script>
<?php }?><?php }} ?>
