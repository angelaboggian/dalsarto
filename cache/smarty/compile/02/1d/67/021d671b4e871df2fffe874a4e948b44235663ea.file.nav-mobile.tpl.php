<?php /* Smarty version Smarty-3.1.19, created on 2017-12-20 14:44:05
         compiled from "/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/blockuserinfo_mod/views/templates/hook/nav-mobile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5874196025a3a6925867135-32246440%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '021d671b4e871df2fffe874a4e948b44235663ea' => 
    array (
      0 => '/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/blockuserinfo_mod/views/templates/hook/nav-mobile.tpl',
      1 => 1512756515,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5874196025a3a6925867135-32246440',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang_id' => 0,
    'is_logged' => 0,
    'welcome_logged' => 0,
    'welcome_link' => 0,
    'link' => 0,
    'show_user_info_icons' => 0,
    'cookie' => 0,
    'welcome' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3a69258d3ec3_42379706',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3a69258d3ec3_42379706')) {function content_5a3a69258d3ec3_42379706($_smarty_tpl) {?><!-- Block user information module NAV  -->
<?php $_smarty_tpl->tpl_vars['show_user_info_icons'] = new Smarty_variable(Configuration::get('ST_SHOW_USER_INFO_ICONS'), null, 0);?>
<?php $_smarty_tpl->tpl_vars['welcome_logged'] = new Smarty_variable(Configuration::get('STSN_WELCOME_LOGGED',$_smarty_tpl->tpl_vars['lang_id']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['welcome_link'] = new Smarty_variable(Configuration::get('STSN_WELCOME_LINK',$_smarty_tpl->tpl_vars['lang_id']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['welcome'] = new Smarty_variable(Configuration::get('STSN_WELCOME',$_smarty_tpl->tpl_vars['lang_id']->value), null, 0);?>
<ul id="userinfo_mod_mobile_menu" class="mo_mu_level_0 mobile_menu_ul">
<?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?>
	<?php if (isset($_smarty_tpl->tpl_vars['welcome_logged']->value)&&trim($_smarty_tpl->tpl_vars['welcome_logged']->value)) {?>
    <li class="mo_ml_level_0 mo_ml_column">
        <a href="<?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?><?php echo $_smarty_tpl->tpl_vars['welcome_link']->value;?>
<?php } else { ?>javascript:;<?php }?>" rel="nofollow" class="mo_ma_level_0 <?php if (!$_smarty_tpl->tpl_vars['welcome_link']->value) {?> ma_span<?php }?>" title="<?php echo $_smarty_tpl->tpl_vars['welcome_logged']->value;?>
">
            <?php echo $_smarty_tpl->tpl_vars['welcome_logged']->value;?>

        </a>
    </li>
    <?php }?>
    <li class="mo_ml_level_0 mo_ml_column">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" class="mo_ma_level_0" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
            <?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_lastname;?>

        </a>
    </li>
    <li class="mo_ml_level_0 mo_ml_column">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" class="mo_ma_level_0" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
            <?php echo smartyTranslate(array('s'=>'My Account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>

        </a>
    </li>
    <li class="mo_ml_level_0 mo_ml_column">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,'mylogout'), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" class="mo_ma_level_0" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
            <?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-logout icon-large"></i><?php }?><?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>

        </a>
    </li>
<?php } else { ?>
	<?php if (isset($_smarty_tpl->tpl_vars['welcome']->value)&&trim($_smarty_tpl->tpl_vars['welcome']->value)) {?>
    <li class="mo_ml_level_0 mo_ml_column">
        <a href="<?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?><?php echo $_smarty_tpl->tpl_vars['welcome_link']->value;?>
<?php } else { ?>javascript:;<?php }?>" rel="nofollow" class="mo_ma_level_0 <?php if (!$_smarty_tpl->tpl_vars['welcome_link']->value) {?> ma_span<?php }?>" title="<?php echo $_smarty_tpl->tpl_vars['welcome']->value;?>
">
            <?php echo $_smarty_tpl->tpl_vars['welcome']->value;?>

        </a>
    </li>
    <?php }?>
    <li class="mo_ml_level_0 mo_ml_column">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Log in to your customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" rel="nofollow" class="mo_ma_level_0">
            <?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo smartyTranslate(array('s'=>'Login','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>

        </a>
    </li>
<?php }?>
</ul>
<!-- /Block usmodule NAV -->
<?php }} ?>
