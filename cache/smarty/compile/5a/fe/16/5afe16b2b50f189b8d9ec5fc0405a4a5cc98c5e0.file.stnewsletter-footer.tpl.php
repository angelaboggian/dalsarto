<?php /* Smarty version Smarty-3.1.19, created on 2017-12-20 14:44:05
         compiled from "/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/stnewsletter/views/templates/hook/stnewsletter-footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3253903125a3a6925148e25-61465071%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5afe16b2b50f189b8d9ec5fc0405a4a5cc98c5e0' => 
    array (
      0 => '/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/stnewsletter/views/templates/hook/stnewsletter-footer.tpl',
      1 => 1512756515,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3253903125a3a6925148e25-61465071',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'news_letter_array' => 0,
    'ec' => 0,
    'content_dir' => 0,
    'gender' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3a69251f2a83_58637934',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3a69251f2a83_58637934')) {function content_5a3a69251f2a83_58637934($_smarty_tpl) {?>
<!-- Block Newsletter module-->
<?php if (isset($_smarty_tpl->tpl_vars['news_letter_array']->value)&&count($_smarty_tpl->tpl_vars['news_letter_array']->value)>0) {?>
    <?php  $_smarty_tpl->tpl_vars['ec'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ec']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['news_letter_array']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ec']->key => $_smarty_tpl->tpl_vars['ec']->value) {
$_smarty_tpl->tpl_vars['ec']->_loop = true;
?>
		<section id="st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
" class="st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
 <?php if ($_smarty_tpl->tpl_vars['ec']->value['hide_on_mobile']) {?> hidden-xs<?php }?> block col-sm-12 col-md-<?php if ($_smarty_tpl->tpl_vars['ec']->value['span']) {?><?php echo $_smarty_tpl->tpl_vars['ec']->value['span'];?>
<?php } else { ?>3<?php }?>">
		    <?php if ($_smarty_tpl->tpl_vars['ec']->value['span']&&$_smarty_tpl->tpl_vars['ec']->value['span']!=12) {?>
    		<div class="title_block"><div class="title_block_name"><?php echo smartyTranslate(array('s'=>'Newsletter','mod'=>'stnewsletter'),$_smarty_tpl);?>
</div><a href="javascript:;" class="opener dlm">&nbsp;</a></div>
			<?php }?>
			<div class="footer_block_content <?php if ($_smarty_tpl->tpl_vars['ec']->value['span']&&$_smarty_tpl->tpl_vars['ec']->value['span']==12) {?>keep_open<?php }?> <?php if ($_smarty_tpl->tpl_vars['ec']->value['text_align']==2) {?> text-center <?php } elseif ($_smarty_tpl->tpl_vars['ec']->value['text_align']==3) {?> text-right <?php }?>">
				<div class="st_news_letter_box">
            	<?php if ($_smarty_tpl->tpl_vars['ec']->value['content']) {?><div class="st_news_letter_content style_content"><?php echo stripslashes($_smarty_tpl->tpl_vars['ec']->value['content']);?>
</div><?php }?>
            	<?php if ($_smarty_tpl->tpl_vars['ec']->value['show_newsletter']) {?>
            	<div class="alert alert-danger hidden"></div>
                <div class="alert alert-success hidden"></div>
            	<form action="<?php echo $_smarty_tpl->tpl_vars['content_dir']->value;?>
/modules/stnewsletter/stnewsletter-ajax.php" method="post" class="st_news_letter_form">
                    <?php if (isset($_smarty_tpl->tpl_vars['ec']->value['show_gender'])&&$_smarty_tpl->tpl_vars['ec']->value['show_gender']) {?>
                        <div class="st_news_letter_gender">
                        <?php  $_smarty_tpl->tpl_vars['gender'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gender']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = Gender::getGenders(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gender']->key => $_smarty_tpl->tpl_vars['gender']->value) {
$_smarty_tpl->tpl_vars['gender']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['gender']->key;
?>
                        <div class="radio-inline">
                            <label for="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" class="top">
                            <input type="radio" name="id_gender" id="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" value="<?php echo intval($_smarty_tpl->tpl_vars['gender']->value->id);?>
" <?php if (isset($_POST['id_gender'])&&$_POST['id_gender']==$_smarty_tpl->tpl_vars['gender']->value->id) {?>checked="checked"<?php }?> />
                            <?php echo $_smarty_tpl->tpl_vars['gender']->value->name;?>
</label>
                        </div>
                        <?php } ?>
                        </div>
                    <?php }?>
					<div class="form-group st_news_letter_form_inner" >
						<input class="inputNew form-control st_news_letter_input" type="text" name="email" size="18" value="<?php if (isset($_smarty_tpl->tpl_vars['value']->value)&&$_smarty_tpl->tpl_vars['value']->value) {?><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<?php }?>" placeholder="<?php echo smartyTranslate(array('s'=>'Your e-mail','mod'=>'stnewsletter'),$_smarty_tpl);?>
" />
		                <button type="submit" name="submitStNewsletter" class="btn btn-medium st_news_letter_submit">
		                    <?php echo smartyTranslate(array('s'=>'Go!','mod'=>'stnewsletter'),$_smarty_tpl);?>

		                </button>
						<input type="hidden" name="action" value="0" />
					</div>
				</form>
				<?php }?>
				</div>
			</div>
		</section>
    <?php } ?>
<?php }?>
<!-- /Block Newsletter module-->
<script type="text/javascript">
	var wrongemailaddress_stnewsletter = "<?php echo smartyTranslate(array('s'=>'Invalid email address.','mod'=>'stnewsletter','js'=>1),$_smarty_tpl);?>
";
</script><?php }} ?>
