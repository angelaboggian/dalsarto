<?php /* Smarty version Smarty-3.1.19, created on 2017-12-20 14:44:05
         compiled from "/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/stinstagram/views/templates/hook/stinstagram.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8824647125a3a6925588d96-78135085%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e7ab84f96dcc3bfc9a509d4f8c3cf0533dfea0f8' => 
    array (
      0 => '/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/stinstagram/views/templates/hook/stinstagram.tpl',
      1 => 1512756515,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8824647125a3a6925588d96-78135085',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'column_slider' => 0,
    'hide_mob' => 0,
    'homeverybottom' => 0,
    'ins_items_fw' => 0,
    'ins_grid' => 0,
    'ins_title_position' => 0,
    'ins_direction_nav' => 0,
    'ins_count' => 0,
    'ins_show_likes' => 0,
    'ins_show_comments' => 0,
    'ins_show_username' => 0,
    'ins_show_timestamp' => 0,
    'ins_show_caption' => 0,
    'ins_lenght_of_caption' => 0,
    'ins_image_size' => 0,
    'ins_hover_effect' => 0,
    'ins_click_action' => 0,
    'ins_slideshow' => 0,
    'ins_slider_s_speed' => 0,
    'ins_control_nav' => 0,
    'ins_rewind_nav' => 0,
    'ins_move' => 0,
    'sttheme' => 0,
    'ins_items_xl' => 0,
    'ins_items_lg' => 0,
    'ins_items_md' => 0,
    'ins_items_sm' => 0,
    'ins_items_xs' => 0,
    'ins_items_xxs' => 0,
    'ins_slider_a_speed' => 0,
    'ins_slider_pause_on_hover' => 0,
    'ins_show_image' => 0,
    'ins_hash_tag' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3a692568d3a0_05608005',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3a692568d3a0_05608005')) {function content_5a3a692568d3a0_05608005($_smarty_tpl) {?>

<!-- MODULE ST instagram -->
<?php $_smarty_tpl->_capture_stack[0][] = array("column_slider", null, null); ob_start(); ?><?php if (isset($_smarty_tpl->tpl_vars['column_slider']->value)&&$_smarty_tpl->tpl_vars['column_slider']->value) {?>_column<?php }?><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<div id="instagram_block_center_container<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
" class="instagram_block_center_container block <?php if ($_smarty_tpl->tpl_vars['hide_mob']->value==1) {?> hidden-xs<?php } elseif ($_smarty_tpl->tpl_vars['hide_mob']->value==2) {?> visible-xs visible-xs-block<?php }?>">
<?php if (isset($_smarty_tpl->tpl_vars['homeverybottom']->value)&&$_smarty_tpl->tpl_vars['homeverybottom']->value&&!$_smarty_tpl->tpl_vars['ins_items_fw']->value) {?><div class="wide_container"><div class="container"><?php }?>
<section id="instagram_block_center<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
" class="instagram_block_center<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
 <?php if (isset($_smarty_tpl->tpl_vars['column_slider']->value)&&$_smarty_tpl->tpl_vars['column_slider']->value) {?> column_block <?php }?> section <?php if ($_smarty_tpl->tpl_vars['ins_grid']->value==1) {?> ins_grid <?php }?>">
    <?php if ($_smarty_tpl->tpl_vars['ins_title_position']->value||(isset($_smarty_tpl->tpl_vars['column_slider']->value)&&$_smarty_tpl->tpl_vars['column_slider']->value)) {?><h3 class="title_block <?php if ((!isset($_smarty_tpl->tpl_vars['column_slider']->value)||!$_smarty_tpl->tpl_vars['column_slider']->value)&&$_smarty_tpl->tpl_vars['ins_title_position']->value==2) {?> title_block_center <?php }?>"><?php echo smartyTranslate(array('s'=>'Follow us on Instagram','mod'=>'stinstagram'),$_smarty_tpl);?>
</h3><?php }?>
    <div id="instagram_block<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
" class="block_content">
        <?php if ($_smarty_tpl->tpl_vars['ins_grid']->value) {?>
        <ul class="instagram_con com_grid_view row ins_connecting">
        </ul>
        <?php } else { ?>
        <div class="instagram_con slides remove_after_init <?php if ($_smarty_tpl->tpl_vars['ins_direction_nav']->value>1) {?> owl-navigation-lr <?php if ($_smarty_tpl->tpl_vars['ins_direction_nav']->value==4) {?> owl-navigation-circle <?php } else { ?> owl-navigation-rectangle <?php }?> <?php } elseif ($_smarty_tpl->tpl_vars['ins_direction_nav']->value==1) {?> owl-navigation-tr<?php }?> ins_connecting">
        </div>
        <?php }?>
        <div class="warning hidden"><?php echo smartyTranslate(array('s'=>'No pictures','mod'=>'stinstagram'),$_smarty_tpl);?>
</div>
    </div>
    <script type="text/javascript">
    //<![CDATA[
    
    jQuery(function($) { 
        $("#instagram_block<?php echo Smarty::$_smarty_vars['capture']['column_slider'];?>
 .instagram_con").pongstgrm({ 
            
            accessToken: '<?php echo Configuration::get('ST_INSTAGRAM_ACCESS_TOKEN');?>
',
            count: <?php if ($_smarty_tpl->tpl_vars['ins_count']->value) {?><?php echo $_smarty_tpl->tpl_vars['ins_count']->value;?>
<?php } else { ?>8<?php }?>,
            grid: <?php if ($_smarty_tpl->tpl_vars['ins_grid']->value) {?>1<?php } else { ?>0<?php }?>,
            likes: <?php if ($_smarty_tpl->tpl_vars['ins_show_likes']->value) {?>1<?php } else { ?>0<?php }?>,       
            comments: <?php if ($_smarty_tpl->tpl_vars['ins_show_comments']->value) {?>1<?php } else { ?>0<?php }?>,    
            username: <?php if ($_smarty_tpl->tpl_vars['ins_show_username']->value) {?>1<?php } else { ?>0<?php }?>,   
            timestamp: <?php if ($_smarty_tpl->tpl_vars['ins_show_timestamp']->value) {?>1<?php } else { ?>0<?php }?>,   
            caption: <?php echo $_smarty_tpl->tpl_vars['ins_show_caption']->value;?>
,   
            ins_lenght_of_caption: <?php echo $_smarty_tpl->tpl_vars['ins_lenght_of_caption']->value;?>
,   
            image_size: <?php echo $_smarty_tpl->tpl_vars['ins_image_size']->value;?>
,
            effects: <?php echo $_smarty_tpl->tpl_vars['ins_hover_effect']->value;?>
,
            click_action: <?php echo $_smarty_tpl->tpl_vars['ins_click_action']->value;?>
,
            
            owl: {
            
                autoPlay : <?php if ($_smarty_tpl->tpl_vars['ins_slideshow']->value) {?><?php echo (($tmp = @$_smarty_tpl->tpl_vars['ins_slider_s_speed']->value)===null||$tmp==='' ? 5000 : $tmp);?>
<?php } else { ?>false<?php }?>,
                navigation: <?php if ($_smarty_tpl->tpl_vars['ins_direction_nav']->value) {?>true<?php } else { ?>false<?php }?>,
                pagination: <?php if ($_smarty_tpl->tpl_vars['ins_control_nav']->value) {?>true<?php } else { ?>false<?php }?>,
                rewindNav: <?php if ($_smarty_tpl->tpl_vars['ins_rewind_nav']->value) {?>true<?php } else { ?>false<?php }?>,
                scrollPerPage: <?php if ($_smarty_tpl->tpl_vars['ins_move']->value) {?>true<?php } else { ?>false<?php }?>,
                
                itemsCustom : [
                    
                    <?php if ($_smarty_tpl->tpl_vars['sttheme']->value['responsive']&&!$_smarty_tpl->tpl_vars['sttheme']->value['version_switching']) {?>
                    <?php if (isset($_smarty_tpl->tpl_vars['homeverybottom']->value)&&$_smarty_tpl->tpl_vars['homeverybottom']->value&&$_smarty_tpl->tpl_vars['ins_items_fw']->value) {?>[<?php if ($_smarty_tpl->tpl_vars['sttheme']->value['responsive_max']==2) {?>1660<?php } else { ?>1420<?php }?>, <?php echo $_smarty_tpl->tpl_vars['ins_items_fw']->value;?>
],<?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['sttheme']->value['responsive_max']==2) {?>[1420, <?php echo $_smarty_tpl->tpl_vars['ins_items_xl']->value;?>
],<?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['sttheme']->value['responsive_max']>=1) {?>[1180, <?php echo $_smarty_tpl->tpl_vars['ins_items_lg']->value;?>
],<?php }?>
                    
                    [972, <?php echo $_smarty_tpl->tpl_vars['ins_items_md']->value;?>
],
                    [748, <?php echo $_smarty_tpl->tpl_vars['ins_items_sm']->value;?>
],
                    [460, <?php echo $_smarty_tpl->tpl_vars['ins_items_xs']->value;?>
],
                    [0, <?php echo $_smarty_tpl->tpl_vars['ins_items_xxs']->value;?>
]
                    <?php } else { ?>
                    [0, <?php if ($_smarty_tpl->tpl_vars['sttheme']->value['responsive_max']==2) {?><?php echo $_smarty_tpl->tpl_vars['ins_items_xl']->value;?>
<?php } elseif ($_smarty_tpl->tpl_vars['sttheme']->value['responsive_max']==1) {?><?php echo $_smarty_tpl->tpl_vars['ins_items_lg']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['ins_items_md']->value;?>
<?php }?>]
                    
                    <?php }?>
                     
                ],
                
                slideSpeed: <?php echo (($tmp = @$_smarty_tpl->tpl_vars['ins_slider_a_speed']->value)===null||$tmp==='' ? 200 : $tmp);?>
,
                stopOnHover: <?php if ($_smarty_tpl->tpl_vars['ins_slider_pause_on_hover']->value) {?>true<?php } else { ?>false<?php }?>
            
            },
            
            ins_items_xl       : <?php if ($_smarty_tpl->tpl_vars['ins_items_xl']->value) {?><?php echo $_smarty_tpl->tpl_vars['ins_items_xl']->value;?>
<?php } else { ?>6<?php }?>,
            ins_items_lg       : <?php if ($_smarty_tpl->tpl_vars['ins_items_lg']->value) {?><?php echo $_smarty_tpl->tpl_vars['ins_items_lg']->value;?>
<?php } else { ?>5<?php }?>,
            ins_items_md       : <?php if ($_smarty_tpl->tpl_vars['ins_items_md']->value) {?><?php echo $_smarty_tpl->tpl_vars['ins_items_md']->value;?>
<?php } else { ?>4<?php }?>,
            ins_items_sm       : <?php if ($_smarty_tpl->tpl_vars['ins_items_sm']->value) {?><?php echo $_smarty_tpl->tpl_vars['ins_items_sm']->value;?>
<?php } else { ?>3<?php }?>,
            ins_items_xs       : <?php if ($_smarty_tpl->tpl_vars['ins_items_xs']->value) {?><?php echo $_smarty_tpl->tpl_vars['ins_items_xs']->value;?>
<?php } else { ?>2<?php }?>,
            ins_items_xxs      : <?php if ($_smarty_tpl->tpl_vars['ins_items_xxs']->value) {?><?php echo $_smarty_tpl->tpl_vars['ins_items_xxs']->value;?>
<?php } else { ?>1<?php }?>,
            show: <?php if ($_smarty_tpl->tpl_vars['ins_show_image']->value==1) {?>'feed'<?php } elseif ($_smarty_tpl->tpl_vars['ins_show_image']->value==2) {?>'liked'<?php } elseif ($_smarty_tpl->tpl_vars['ins_show_image']->value==3&&$_smarty_tpl->tpl_vars['ins_hash_tag']->value) {?>'<?php echo $_smarty_tpl->tpl_vars['ins_hash_tag']->value;?>
'<?php } else { ?>'recent'<?php }?>
            
        });
    });
     
    //]]>
    </script>
</section>
<?php if (isset($_smarty_tpl->tpl_vars['homeverybottom']->value)&&$_smarty_tpl->tpl_vars['homeverybottom']->value&&!$_smarty_tpl->tpl_vars['ins_items_fw']->value) {?></div></div><?php }?>
</div>
<!-- /MODULE ST instagram --><?php }} ?>
