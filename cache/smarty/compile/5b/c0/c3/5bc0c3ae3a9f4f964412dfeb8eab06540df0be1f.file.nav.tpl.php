<?php /* Smarty version Smarty-3.1.19, created on 2017-12-20 14:44:05
         compiled from "/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/blockuserinfo_mod/views/templates/hook/nav.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2812344945a3a6925cbfec7-20959579%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5bc0c3ae3a9f4f964412dfeb8eab06540df0be1f' => 
    array (
      0 => '/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/blockuserinfo_mod/views/templates/hook/nav.tpl',
      1 => 1512756515,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2812344945a3a6925cbfec7-20959579',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang_id' => 0,
    'is_logged' => 0,
    'userinfo_navleft' => 0,
    'welcome_logged' => 0,
    'welcome_link' => 0,
    'show_welcome_msg' => 0,
    'userinfo_dropdown' => 0,
    'link' => 0,
    'show_user_info_icons' => 0,
    'cookie' => 0,
    'welcome' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3a6925e4bb94_39964330',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3a6925e4bb94_39964330')) {function content_5a3a6925e4bb94_39964330($_smarty_tpl) {?><!-- Block user information module NAV  -->
<?php $_smarty_tpl->tpl_vars['userinfo_dropdown'] = new Smarty_variable(Configuration::get('ST_USERINFO_DROPDOWN'), null, 0);?>
<?php $_smarty_tpl->tpl_vars['show_user_info_icons'] = new Smarty_variable(Configuration::get('ST_SHOW_USER_INFO_ICONS'), null, 0);?>
<?php $_smarty_tpl->tpl_vars['welcome_logged'] = new Smarty_variable(Configuration::get('STSN_WELCOME_LOGGED',$_smarty_tpl->tpl_vars['lang_id']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['welcome_link'] = new Smarty_variable(Configuration::get('STSN_WELCOME_LINK',$_smarty_tpl->tpl_vars['lang_id']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['welcome'] = new Smarty_variable(Configuration::get('STSN_WELCOME',$_smarty_tpl->tpl_vars['lang_id']->value), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?>
	<?php if (isset($_smarty_tpl->tpl_vars['userinfo_navleft']->value)&&$_smarty_tpl->tpl_vars['userinfo_navleft']->value) {?>
		<?php if (isset($_smarty_tpl->tpl_vars['welcome_logged']->value)&&trim($_smarty_tpl->tpl_vars['welcome_logged']->value)) {?><?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?><a href="<?php echo $_smarty_tpl->tpl_vars['welcome_link']->value;?>
" class="welcome top_bar_item <?php if (!isset($_smarty_tpl->tpl_vars['show_welcome_msg']->value)||!$_smarty_tpl->tpl_vars['show_welcome_msg']->value) {?> hidden_extra_small <?php }?>" rel="nofollow" title="<?php echo $_smarty_tpl->tpl_vars['welcome_logged']->value;?>
"><?php } else { ?><span class="welcome top_bar_item <?php if (!isset($_smarty_tpl->tpl_vars['show_welcome_msg']->value)||!$_smarty_tpl->tpl_vars['show_welcome_msg']->value) {?> hidden_extra_small <?php }?>"><?php }?><span class="header_item"><?php echo $_smarty_tpl->tpl_vars['welcome_logged']->value;?>
</span><?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?></a><?php } else { ?></span><?php }?><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['userinfo_dropdown']->value) {?>
			<div class="userinfo_mod_top dropdown_wrap top_bar_item">
		        <div class="dropdown_tri dropdown_tri_in header_item">
		            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" rel="nofollow">
		        		<?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_lastname;?>

		            </a>
		        </div>
		        <div class="dropdown_list">
            		<ul class="dropdown_list_ul custom_links_list">
            			<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My Account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</a></li>
						<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,'mylogout'), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</a></li>
		    		</ul>
		        </div>
		    </div>
		<?php } else { ?>
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" class="account top_bar_item" rel="nofollow"><span class="header_item"><?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_lastname;?>
</span></a>
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" class="my_account_link top_bar_item" rel="nofollow"><span class="header_item"><?php echo smartyTranslate(array('s'=>'My Account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</span></a>
			<a class="logout top_bar_item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,'mylogout'), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
				<span class="header_item"><?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-logout icon-large"></i><?php }?><?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</span>
			</a>
		<?php }?>
	<?php } else { ?>
		<?php if ($_smarty_tpl->tpl_vars['userinfo_dropdown']->value) {?>
			<div class="userinfo_mod_top dropdown_wrap top_bar_item">
		        <div class="dropdown_tri dropdown_tri_in header_item">
		            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" rel="nofollow">
		        		<?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_lastname;?>

		            </a>
		        </div>
		        <div class="dropdown_list">
            		<ul class="dropdown_list_ul custom_links_list">
            			<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My Account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</a></li>
						<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,'mylogout'), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</a></li>
		    		</ul>
		        </div>
		    </div>
		<?php } else { ?>
			<a class="logout top_bar_item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,'mylogout'), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
				<span class="header_item"><?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-logout icon-large"></i><?php }?><?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</span>
			</a>
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" class="my_account_link top_bar_item" rel="nofollow"><span class="header_item"><?php echo smartyTranslate(array('s'=>'My Account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</span></a>
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
" class="account top_bar_item" rel="nofollow"><span class="header_item"><?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_lastname;?>
</span></a>
		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['welcome_logged']->value)&&trim($_smarty_tpl->tpl_vars['welcome_logged']->value)) {?><?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?><a href="<?php echo $_smarty_tpl->tpl_vars['welcome_link']->value;?>
" class="welcome top_bar_item <?php if (!isset($_smarty_tpl->tpl_vars['show_welcome_msg']->value)||!$_smarty_tpl->tpl_vars['show_welcome_msg']->value) {?> hidden_extra_small <?php }?>" rel="nofollow" title="<?php echo $_smarty_tpl->tpl_vars['welcome_logged']->value;?>
"><?php } else { ?><span class="welcome top_bar_item <?php if (!isset($_smarty_tpl->tpl_vars['show_welcome_msg']->value)||!$_smarty_tpl->tpl_vars['show_welcome_msg']->value) {?> hidden_extra_small <?php }?>"><?php }?><span class="header_item"><?php echo $_smarty_tpl->tpl_vars['welcome_logged']->value;?>
</span><?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?></a><?php } else { ?></span><?php }?><?php }?>
	<?php }?>
	<div id="for_mobile_userinfo" class="hidden">
		<?php if (isset($_smarty_tpl->tpl_vars['welcome_logged']->value)&&trim($_smarty_tpl->tpl_vars['welcome_logged']->value)) {?>
	    <li class="mo_ml_level_0 mo_ml_column">
	        <a href="<?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?><?php echo $_smarty_tpl->tpl_vars['welcome_link']->value;?>
<?php } else { ?>javascript:;<?php }?>" rel="nofollow" class="mo_ma_level_0 <?php if (!$_smarty_tpl->tpl_vars['welcome_link']->value) {?> ma_span<?php }?>" title="<?php echo $_smarty_tpl->tpl_vars['welcome_logged']->value;?>
">
	            <?php echo $_smarty_tpl->tpl_vars['welcome_logged']->value;?>

	        </a>
	    </li>
	    <?php }?>
	    <li class="mo_ml_level_0 mo_ml_column">
	        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" class="mo_ma_level_0" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
	            <?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_lastname;?>

	        </a>
	    </li>
	    <li class="mo_ml_level_0 mo_ml_column">
	        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" class="mo_ma_level_0" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
	            <?php echo smartyTranslate(array('s'=>'My Account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>

	        </a>
	    </li>
	    <li class="mo_ml_level_0 mo_ml_column">
	        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,'mylogout'), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" class="mo_ma_level_0" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
	            <?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-logout icon-large"></i><?php }?><?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>

	        </a>
	    </li>
	</div>
<?php } else { ?>
	<?php if (isset($_smarty_tpl->tpl_vars['userinfo_navleft']->value)&&$_smarty_tpl->tpl_vars['userinfo_navleft']->value) {?>
		<?php if (isset($_smarty_tpl->tpl_vars['welcome']->value)&&trim($_smarty_tpl->tpl_vars['welcome']->value)) {?><?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?><a href="<?php echo $_smarty_tpl->tpl_vars['welcome_link']->value;?>
" class="welcome top_bar_item <?php if (!isset($_smarty_tpl->tpl_vars['show_welcome_msg']->value)||!$_smarty_tpl->tpl_vars['show_welcome_msg']->value) {?> hidden_extra_small <?php }?>" rel="nofollow" title="<?php echo $_smarty_tpl->tpl_vars['welcome']->value;?>
"><?php } else { ?><span class="welcome top_bar_item <?php if (!isset($_smarty_tpl->tpl_vars['show_welcome_msg']->value)||!$_smarty_tpl->tpl_vars['show_welcome_msg']->value) {?> hidden_extra_small <?php }?>"><?php }?><span class="header_item"><?php echo $_smarty_tpl->tpl_vars['welcome']->value;?>
</span><?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?></a><?php } else { ?></span><?php }?><?php }?>
		<a class="login top_bar_item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log in to your customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
			<span class="header_item"><?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo smartyTranslate(array('s'=>'Login','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</span>
		</a>
	<?php } else { ?>
		<a class="login top_bar_item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log in to your customer account','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
">
			<span class="header_item"><?php if ($_smarty_tpl->tpl_vars['show_user_info_icons']->value) {?><i class="icon-user-1 icon-mar-lr2 icon-large"></i><?php }?><?php echo smartyTranslate(array('s'=>'Login','mod'=>'blockuserinfo_mod'),$_smarty_tpl);?>
</span>
		</a>
		<?php if (isset($_smarty_tpl->tpl_vars['welcome']->value)&&trim($_smarty_tpl->tpl_vars['welcome']->value)) {?><?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?><a href="<?php echo $_smarty_tpl->tpl_vars['welcome_link']->value;?>
" class="welcome top_bar_item <?php if (!isset($_smarty_tpl->tpl_vars['show_welcome_msg']->value)||!$_smarty_tpl->tpl_vars['show_welcome_msg']->value) {?> hidden_extra_small <?php }?>" rel="nofollow" title="<?php echo $_smarty_tpl->tpl_vars['welcome']->value;?>
"><?php } else { ?><span class="welcome top_bar_item <?php if (!isset($_smarty_tpl->tpl_vars['show_welcome_msg']->value)||!$_smarty_tpl->tpl_vars['show_welcome_msg']->value) {?> hidden_extra_small <?php }?>"><?php }?><span class="header_item"><?php echo $_smarty_tpl->tpl_vars['welcome']->value;?>
</span><?php if ($_smarty_tpl->tpl_vars['welcome_link']->value) {?></a><?php } else { ?></span><?php }?><?php }?>
	<?php }?>
<?php }?>
<!-- /Block usmodule NAV -->
<?php }} ?>
