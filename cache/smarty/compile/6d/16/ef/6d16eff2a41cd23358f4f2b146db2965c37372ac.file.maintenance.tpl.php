<?php /* Smarty version Smarty-3.1.19, created on 2017-12-20 15:03:12
         compiled from "/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/themes/dalsarto/maintenance.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7373123005a3a6da0bae415-73245443%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d16eff2a41cd23358f4f2b146db2965c37372ac' => 
    array (
      0 => '/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/themes/dalsarto/maintenance.tpl',
      1 => 1512756514,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7373123005a3a6da0bae415-73245443',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'language_code' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'nobots' => 0,
    'favicon_url' => 0,
    'css_dir' => 0,
    'HOOK_COMING_SOON' => 0,
    'logo_url' => 0,
    'logo_image_width' => 0,
    'logo_image_height' => 0,
    'HOOK_MAINTENANCE' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3a6da0c35ba3_97823216',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3a6da0c35ba3_97823216')) {function content_5a3a6da0c35ba3_97823216($_smarty_tpl) {?>
<!DOCTYPE html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language_code']->value, ENT_QUOTES, 'UTF-8', true);?>
">
<head>
	<meta charset="utf-8">
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
<?php if (isset($_smarty_tpl->tpl_vars['meta_description']->value)) {?>
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
">
<?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['meta_keywords']->value)) {?>
	<meta name="keywords" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
">
<?php }?>
	<meta name="robots" content="<?php if (isset($_smarty_tpl->tpl_vars['nobots']->value)) {?>no<?php }?>index,follow">
	<link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['favicon_url']->value;?>
">
   	<link href="<?php echo $_smarty_tpl->tpl_vars['css_dir']->value;?>
maintenance.css" rel="stylesheet">
</head>
<body>
	<?php if (!isset($_smarty_tpl->tpl_vars['HOOK_COMING_SOON']->value)) {?>
		<?php $_smarty_tpl->_capture_stack[0][] = array("displayComingSoon", null, null); ob_start(); ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayComingSoon"),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php }?>
	<?php if ((isset($_smarty_tpl->tpl_vars['HOOK_COMING_SOON']->value)&&trim($_smarty_tpl->tpl_vars['HOOK_COMING_SOON']->value))||(isset(Smarty::$_smarty_vars['capture']['displayComingSoon'])&&trim(Smarty::$_smarty_vars['capture']['displayComingSoon']))) {?>
		<div id="coming_soon_container">
			<?php if (isset($_smarty_tpl->tpl_vars['HOOK_COMING_SOON']->value)) {?><?php echo $_smarty_tpl->tpl_vars['HOOK_COMING_SOON']->value;?>
<?php }?>
			<?php if (isset(Smarty::$_smarty_vars['capture']['displayComingSoon'])) {?><?php echo Smarty::$_smarty_vars['capture']['displayComingSoon'];?>
<?php }?>			
		</div>
	<?php } else { ?>
	<div class="container">
		<div id="maintenance">
			<div class="logo"><img src="<?php echo $_smarty_tpl->tpl_vars['logo_url']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['logo_image_width']->value) {?>width="<?php echo $_smarty_tpl->tpl_vars['logo_image_width']->value;?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['logo_image_height']->value) {?>height="<?php echo $_smarty_tpl->tpl_vars['logo_image_height']->value;?>
"<?php }?> alt="logo" /></div>
        		<?php echo $_smarty_tpl->tpl_vars['HOOK_MAINTENANCE']->value;?>

        		<div id="message">
         			<h1 class="maintenance-heading"><?php echo smartyTranslate(array('s'=>'We\'ll be back soon.'),$_smarty_tpl);?>
</h1>
					<?php echo smartyTranslate(array('s'=>'We are currently updating our shop and will be back really soon.'),$_smarty_tpl);?>

					<br />
					<?php echo smartyTranslate(array('s'=>'Thanks for your patience.'),$_smarty_tpl);?>

				</div>
		</div>
    </div>
	<?php }?>
</body>
</html>
<?php }} ?>
