<?php /* Smarty version Smarty-3.1.19, created on 2017-12-20 14:44:06
         compiled from "/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/stnewsletter/views/templates/hook/stnewsletter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8239551055a3a6926317e49-54268172%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f2b24ba523fbcdec8322133d7c754c8711780a2' => 
    array (
      0 => '/var/www/vhosts/vm6660.seewebcloud.it/test.dalsarto.com/modules/stnewsletter/views/templates/hook/stnewsletter.tpl',
      1 => 1512756515,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8239551055a3a6926317e49-54268172',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'news_letter_array' => 0,
    'ec' => 0,
    'has_news_letter_popup' => 0,
    'content_dir' => 0,
    'gender' => 0,
    'value' => 0,
    'news_letter_cookie_domain' => 0,
    'news_letter_cookie_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3a6926465f61_69455741',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3a6926465f61_69455741')) {function content_5a3a6926465f61_69455741($_smarty_tpl) {?>
<?php if (isset($_smarty_tpl->tpl_vars['news_letter_array']->value)&&count($_smarty_tpl->tpl_vars['news_letter_array']->value)>0) {?>
	<?php $_smarty_tpl->tpl_vars['has_news_letter_popup'] = new Smarty_variable(0, null, 0);?>
    <?php  $_smarty_tpl->tpl_vars['ec'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ec']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['news_letter_array']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ec']->key => $_smarty_tpl->tpl_vars['ec']->value) {
$_smarty_tpl->tpl_vars['ec']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['ec']->value['location']==4) {?>
        	<?php if (!$_smarty_tpl->tpl_vars['has_news_letter_popup']->value) {?>
        		<?php $_smarty_tpl->tpl_vars['has_news_letter_popup'] = new Smarty_variable(1, null, 0);?>
        		<div class="st_news_letter_popup_wrap">
        		<div id="st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
" class="st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
 st_news_letter st_news_letter_popup <?php if ($_smarty_tpl->tpl_vars['ec']->value['text_align']==2) {?> text-center <?php } elseif ($_smarty_tpl->tpl_vars['ec']->value['text_align']==3) {?> text-right <?php }?>">
         	      <div class="st_news_letter_box">
                    <div class="alert alert-danger hidden"></div>
                    <div class="alert alert-success hidden"></div>                   
	            	<?php if ($_smarty_tpl->tpl_vars['ec']->value['content']) {?><div class="st_news_letter_content style_content"><?php echo stripslashes($_smarty_tpl->tpl_vars['ec']->value['content']);?>
</div><?php }?>
	            	<?php if ($_smarty_tpl->tpl_vars['ec']->value['show_newsletter']) {?>
	            	<form action="<?php echo $_smarty_tpl->tpl_vars['content_dir']->value;?>
/modules/stnewsletter/stnewsletter-ajax.php" method="post" class="st_news_letter_form">
	            		<?php if (isset($_smarty_tpl->tpl_vars['ec']->value['show_gender'])&&$_smarty_tpl->tpl_vars['ec']->value['show_gender']) {?>
                            <div class="st_news_letter_gender">
                            <?php  $_smarty_tpl->tpl_vars['gender'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gender']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = Gender::getGenders(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gender']->key => $_smarty_tpl->tpl_vars['gender']->value) {
$_smarty_tpl->tpl_vars['gender']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['gender']->key;
?>
                            <div class="radio-inline">
                                <label for="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" class="top">
                                <input type="radio" name="id_gender" id="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" value="<?php echo intval($_smarty_tpl->tpl_vars['gender']->value->id);?>
" <?php if (isset($_POST['id_gender'])&&$_POST['id_gender']==$_smarty_tpl->tpl_vars['gender']->value->id) {?>checked="checked"<?php }?> />
                                <?php echo $_smarty_tpl->tpl_vars['gender']->value->name;?>
</label>
                            </div>
                            <?php } ?>
                            </div>
                        <?php }?>
						<div class="form-group st_news_letter_form_inner" >
							<input class="inputNew form-control st_news_letter_input" type="text" name="email" size="18" value="<?php if (isset($_smarty_tpl->tpl_vars['value']->value)&&$_smarty_tpl->tpl_vars['value']->value) {?><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<?php }?>" placeholder="<?php echo smartyTranslate(array('s'=>'Your e-mail','mod'=>'stnewsletter'),$_smarty_tpl);?>
" />
			                <button type="submit" name="submitStNewsletter" class="btn btn-medium st_news_letter_submit">
			                    <?php echo smartyTranslate(array('s'=>'Subscribe','mod'=>'stnewsletter'),$_smarty_tpl);?>

			                </button>
							<input type="hidden" name="action" value="0" />
						</div>
					</form>				
					<?php }?>
	            	</div>
					<?php if (!$_smarty_tpl->tpl_vars['ec']->value['show_popup']) {?>
					<div class="st_news_letter_do_not_show_outer clearfix">
                    	<div class="st_news_letter_do_not_show_inner">
                    		<input type="checkbox" name="st_news_letter_do_not_show" class="st_news_letter_do_not_show" autocomplete="off" /><label for="st_news_letter_do_not_show"><?php echo smartyTranslate(array('s'=>'Do not show again','mod'=>'stnewsletter'),$_smarty_tpl);?>
</label>
                    	</div>
					</div>
					<?php }?>	
	            </div>
	            </div>
		        <script type="text/javascript">
		        
		        jQuery(function($){
		            <?php if (!$_smarty_tpl->tpl_vars['ec']->value['delay_popup']) {?>
		            	open_st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
();
		            <?php } else { ?>
		            	setTimeout(open_st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
, <?php echo $_smarty_tpl->tpl_vars['ec']->value['delay_popup'];?>
*1000);
		            <?php }?>
		            <?php if (!$_smarty_tpl->tpl_vars['ec']->value['show_popup']) {?>
		            $('#st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
 .st_news_letter_do_not_show').change(function () {
			            if ($(this).is(':checked')) {
			                $.cookie("st_popup_do_not_show_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
", '<?php echo $_smarty_tpl->tpl_vars['ec']->value['show_popup'];?>
', {
			                    'expires': <?php if ($_smarty_tpl->tpl_vars['ec']->value['cookies_time']) {?><?php echo $_smarty_tpl->tpl_vars['ec']->value['cookies_time'];?>
<?php } else { ?>30<?php }?>,
			                    'domain': '<?php echo $_smarty_tpl->tpl_vars['news_letter_cookie_domain']->value;?>
',
			                    'path': '<?php echo $_smarty_tpl->tpl_vars['news_letter_cookie_path']->value;?>
'
			                });
			            } else {
			                $.cookie("st_popup_do_not_show_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
", '<?php echo $_smarty_tpl->tpl_vars['ec']->value['show_popup'];?>
', {
			                	'expires':-1,
			                    'domain': '<?php echo $_smarty_tpl->tpl_vars['news_letter_cookie_domain']->value;?>
',
			                    'path': '<?php echo $_smarty_tpl->tpl_vars['news_letter_cookie_path']->value;?>
'
			                }); 
			            }
			        });
		            <?php }?>
		            <?php if ($_smarty_tpl->tpl_vars['ec']->value['show_newsletter']) {?>
		            regested_popup = function() {
		                $.cookie("st_popup_do_not_show_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
", '<?php echo $_smarty_tpl->tpl_vars['ec']->value['show_popup'];?>
', {
		                    'domain': '<?php echo $_smarty_tpl->tpl_vars['news_letter_cookie_domain']->value;?>
',
		                    'path': '<?php echo $_smarty_tpl->tpl_vars['news_letter_cookie_path']->value;?>
'
		                });
		            	return true;
		            };
                    <?php if ($_smarty_tpl->tpl_vars['ec']->value['show_popup']==2) {?>
                    $.cookie("st_popup_do_not_show_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
", '<?php echo $_smarty_tpl->tpl_vars['ec']->value['show_popup'];?>
', {
	                    'expires': <?php if ($_smarty_tpl->tpl_vars['ec']->value['cookies_time']) {?><?php echo $_smarty_tpl->tpl_vars['ec']->value['cookies_time'];?>
<?php } else { ?>30<?php }?>,
	                    'domain': '<?php echo $_smarty_tpl->tpl_vars['news_letter_cookie_domain']->value;?>
',
	                    'path': '<?php echo $_smarty_tpl->tpl_vars['news_letter_cookie_path']->value;?>
'
	                });
                    <?php }?>
		            <?php }?>
		        });
		        var open_st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
 = function(){
		        	if (!!$.prototype.fancybox)
			        	$.fancybox({
			            	'padding': '0',
			            	'type': 'inline',
			            	<?php if ($_smarty_tpl->tpl_vars['ec']->value['hide_on_mobile']) {?>
					        'beforeLoad' : function(){
					            if(st_responsive && $(window).width()<=768)
					                return false;
					        },
					        <?php }?>
			                'href': '#st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
'
			            });
		        };
		        
		        </script>
        	<?php }?>
        <?php } else { ?>
	        <?php if (isset($_smarty_tpl->tpl_vars['ec']->value['is_full_width'])&&$_smarty_tpl->tpl_vars['ec']->value['is_full_width']) {?><div id="st_news_letter_container_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
" class="st_news_letter_container full_container <?php if ($_smarty_tpl->tpl_vars['ec']->value['hide_on_mobile']) {?>hidden-xs<?php }?> block"><div class="container"><div class="row"><div class="col-xs-12 col-sm-12"><?php }?>
	            <div id="st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
" class="st_news_letter_<?php echo $_smarty_tpl->tpl_vars['ec']->value['id_st_news_letter'];?>
 <?php if ($_smarty_tpl->tpl_vars['ec']->value['hide_on_mobile']) {?>hidden-xs<?php }?> <?php if (!isset($_smarty_tpl->tpl_vars['ec']->value['is_full_width'])||!$_smarty_tpl->tpl_vars['ec']->value['is_full_width']) {?>block<?php }?> st_news_letter <?php if (isset($_smarty_tpl->tpl_vars['ec']->value['is_column'])&&$_smarty_tpl->tpl_vars['ec']->value['is_column']) {?> column_block <?php }?> <?php if ($_smarty_tpl->tpl_vars['ec']->value['text_align']==2) {?> text-center <?php } elseif ($_smarty_tpl->tpl_vars['ec']->value['text_align']==3) {?> text-right <?php }?>">
	            	<div class="st_news_letter_box">
	            	<?php if ($_smarty_tpl->tpl_vars['ec']->value['content']) {?><div class="st_news_letter_content style_content"><?php echo stripslashes($_smarty_tpl->tpl_vars['ec']->value['content']);?>
</div><?php }?>
	            	<?php if ($_smarty_tpl->tpl_vars['ec']->value['show_newsletter']) {?>
                    <div class="alert alert-danger hidden"></div>
                    <div class="alert alert-success hidden"></div>
	            	<form action="<?php echo $_smarty_tpl->tpl_vars['content_dir']->value;?>
/modules/stnewsletter/stnewsletter-ajax.php" method="post" class="st_news_letter_form">
						<div class="form-group st_news_letter_form_inner" >
                            <?php if ($_smarty_tpl->tpl_vars['ec']->value['show_gender']) {?>
                            <label><?php echo smartyTranslate(array('s'=>'Title'),$_smarty_tpl);?>
</label>
                            <?php  $_smarty_tpl->tpl_vars['gender'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gender']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = Gender::getGenders(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gender']->key => $_smarty_tpl->tpl_vars['gender']->value) {
$_smarty_tpl->tpl_vars['gender']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['gender']->key;
?>
                            <div class="radio-inline">
                                <label for="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" class="top">
                                <input type="radio" name="id_gender" id="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" value="<?php echo intval($_smarty_tpl->tpl_vars['gender']->value->id);?>
" <?php if (isset($_POST['id_gender'])&&$_POST['id_gender']==$_smarty_tpl->tpl_vars['gender']->value->id) {?>checked="checked"<?php }?> />
                                <?php echo $_smarty_tpl->tpl_vars['gender']->value->name;?>
</label>
                            </div>
                            <?php } ?>
                            <?php }?>
							<input class="inputNew form-control st_news_letter_input" type="text" name="email" size="18" value="<?php if (isset($_smarty_tpl->tpl_vars['value']->value)&&$_smarty_tpl->tpl_vars['value']->value) {?><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<?php }?>" placeholder="<?php echo smartyTranslate(array('s'=>'Your e-mail','mod'=>'stnewsletter'),$_smarty_tpl);?>
" />
			                <button type="submit" name="submitStNewsletter" class="btn btn-medium st_news_letter_submit">
			                    <?php echo smartyTranslate(array('s'=>'Subscribe','mod'=>'stnewsletter'),$_smarty_tpl);?>

			                </button>
							<input type="hidden" name="action" value="0" />
						</div>
					</form>
					<?php }?>
					</div>
	            </div>
	        <?php if (isset($_smarty_tpl->tpl_vars['ec']->value['is_full_width'])&&$_smarty_tpl->tpl_vars['ec']->value['is_full_width']) {?></div></div></div></div><?php }?>
        <?php }?>
    <?php } ?>
<?php }?>

<script type="text/javascript">
	var wrongemailaddress_stnewsletter = "<?php echo smartyTranslate(array('s'=>'Invalid email address.','mod'=>'stnewsletter','js'=>1),$_smarty_tpl);?>
";
</script><?php }} ?>
